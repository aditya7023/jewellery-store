## Problem
1. Generating Invoice for Orders
2. Tracking Orders
3. Matching Import duties
4. Handling Shipping Details
5. Saving Receipts

[Existing Workflow](./ExistingWorkflow.wsd)

### MVP

![MVP Features](http://www.plantuml.com/plantuml/proxy?cache=no&src=https://bitbucket.org/aditya7023/jewellery-store/raw/e2ad52c0c1082c9aa1090d34d9ff4d081d79e79e/Docs/MVP.wsd)

### Entity-Relationship Diagram

![ERD](http://www.plantuml.com/plantuml/proxy?cache=no&src=https://bitbucket.org/aditya7023/jewellery-store/raw/e2ad52c0c1082c9aa1090d34d9ff4d081d79e79e/Docs/ERD.wsd)